﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ConexionADONETDB.Persistencias
{
    public class Cliente
    {

       public DataTable ListaConsulta()
        {
            Persistencia persi = new Persistencia();
            DataTable dt = null;
           var conn =  persi.AbrirConexion();
            SqlCommand cm = new SqlCommand("Select * from Listado");
            SqlDataReader reader = cm.ExecuteReader();

            while (reader.Read())
            {
                reader[""].ToString();
                reader[""].ToString();
                reader[""].ToString();
            }
            dt.Load(reader);
            return dt;

            _ = persi.CerrarConexion();
        }

        public string Insertar()
        {
            try
            {
            Persistencia persi = new Persistencia();
            var conn = persi.AbrirConexion();
            SqlCommand cm = new SqlCommand("insert into Clientes_Trans (Cliente_Nombre, Cliente_Apellido, Cliente_Email) values (?,?,?)");
            cm.ExecuteNonQuery();

                return "Se inserto el cliente";

            } catch( Exception ex)
            {
                return "Error. No se pudo agregar al cliente" + ex.Message;
            }


        }

        public string Actualizar()
        {
            try
            {
                Persistencia persi = new Persistencia();
                var conn = persi.AbrirConexion();
                SqlCommand cm = new SqlCommand("update Clientes_Trans set Cliente_Nombre = '', Cliente_Apellido= '', Cliente_Email= '' where cliente_id = ?");
                cm.ExecuteNonQuery();

                return "Se actualizo el cliente";

            }
            catch (Exception ex)
            {
                return "Error. No se pudo actualiza al cliente" + ex.Message;
            }


        }

        public string Eliminar()
        {
            try
            {
                Persistencia persi = new Persistencia();
                var conn = persi.AbrirConexion();
                SqlCommand cm = new SqlCommand("delete from Clientes_Trans where cliente_id = ?");
                cm.ExecuteNonQuery();

                return "Se elimino el cliente";

            }
            catch (Exception ex)
            {
                return "Error. No se pudo eliminar al cliente" + ex.Message;
            }


        }

    }
}