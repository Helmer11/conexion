﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ConexionADONETDB.Persistencias
{
    public class Persistencia
    {
      static private string cadenaConexion = ConfigurationManager.ConnectionStrings["PruebaConexion"].ConnectionString;
        private SqlConnection conec = null;

        public SqlConnection AbrirConexion() 
        {
            conec = new SqlConnection(cadenaConexion);
            if (conec.State == ConnectionState.Closed )
                conec.Open();
            return conec;
        }


        public SqlConnection CerrarConexion()
        {
            if (conec.State == ConnectionState.Open)
                conec.Close();
            return conec;
        }



















    }
}